import requests


class Warn(object):

    def __init__(self):
        self.uri = 'http://sms.xtp.com:5588/'
        self.text_url = self.uri + 'send'
        self.card_url = self.uri + 'sendcard'

    def send_text(self, data):
        requests.post(self.text_url, data=data)

    def send_card(self, team, title, content, url):
        requests.post(self.card_url, data={
            'xtpteam': team,
            'title': title,
            'content': content,
            'url': url
        })

    def etf_add(self, exch, ticker, name, etf_type, update_time):
        self.send_card(
            'etf',
            'ETF添加提醒',
            '<div>exch: %d, ticker: %s, name: %s, type: %s</div><br><div>设定的更新时间为: %s</div>' % (
                exch, ticker, name, etf_type, update_time
            ),
            'http://xrs.xtp.com/#/etf-list'
        )


warn = Warn()
