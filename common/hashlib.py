import os
import hashlib


def md5encode(content):
    obj = hashlib.md5()
    if os.path.exists(content):
        with open(content, 'rb') as f:
            obj.update(f.read())
    else:
        obj.update(content.encode())
    return obj.hexdigest()



