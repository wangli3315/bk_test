# -*- coding: utf-8 -*-
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi
from rest_framework.views import APIView
import json
from django.http import JsonResponse


def get_request_args(func):
    def _get_request_args(self, request):
        if request.method == 'GET':
            args = request.GET
        else:
            body = request.body
            if body:
                try:
                    args = json.loads(body)
                except Exception as e:
                    print(e)
                    # return makeJsonResponse(status=StatusCode.EXECUTE_FAIL, message=str(e))
                    args = request.POST
            else:
                args = request.POST
        return func(self, request, args)

    return _get_request_args

class GetEzoesConfig(APIView):
    '''
        list:
            Return one news
        '''
    test_param = openapi.Parameter("id", openapi.IN_QUERY, description="test manual param",
                                   type=openapi.TYPE_INTEGER)

    @swagger_auto_schema(operation_description="partial_update description override",
                         responses={404: 'id not found'},
                         manual_parameters=[test_param])
    @get_request_args
    def get(self, request, args):
        data = []
        return JsonResponse({"data": data})

    @swagger_auto_schema(
        operation_description="apiview post description override",
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            required=['id', "user_name"],
            properties={
                'id': openapi.Schema(type=openapi.TYPE_INTEGER),
                'user_name': openapi.Schema(type=openapi.TYPE_STRING)

            },
        ),
        security=[]
    )
    @get_request_args
    def post(self, request, args):
        data = []
        id = args.get("id")

        return JsonResponse({"data": data})
        pass

    # def read_ezoes_config_file(file_path):
    #     conf = ConfigParser()
    #     conf.read(file_path)
    #     operList = []
    #     OperatorCounts = conf.get("System", "OperatorCounts")
    #     for key, section_info in conf.items():
    #         if str(key).startswith("oper"):
    #             OperObj = {}
    #             OperObj.update({"OperCode": section_info.get("OperCode")})
    #             OperObj.update({"Gwip1": section_info.get("Gwip1")})
    #             OperObj.update({"LocalIP1": section_info.get("LocalIP1")})
    #             OperObj.update({"Gwip1": section_info.get("Gwip1")})
    #             OperObj.update({"LocalIP1": section_info.get("LocalIP1")})
    #             OperObj.update({"Gwip2": section_info.get("Gwip2")})
    #             OperObj.update({"LocalIP2": section_info.get("LocalIP2")})
    #             OperObj.update({"Gwip3": section_info.get("Gwip3")})
    #             OperObj.update({"LocalIP3": section_info.get("LocalIP3")})
    #             OperObj.update({"DbUser": section_info.get("DbUser")})
    #             OperObj.update({"DbName": section_info.get("jdbc.url").split("=")[-1]})
    #             operList.append(OperObj)
    #     if len(operList) != OperatorCounts:
    #         AssertionError("读取 %s 配置文件数量异常,请检查" % file_path)
    #     return operList
    #
    # def ReloadConfig(request):
    #     conf_path = request.GET.get('path')
    #     if not conf_path:
    #         return HttpResponseNotModified()
    #     operList = read_ezoes_config_file(conf_path)
    #     return HttpResponse(operList)
    #
    # def LoadModel(request,model_name):
    #
    #     return HttpResponse(model_name)