from django.apps import AppConfig


class ReloadCmdbConfig(AppConfig):
    name = 'reload_cmdb'
